package InterfaceGraphique;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JFrame;
import java.awt.Container;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Choice;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 * Ecran principal de l'interface graphique possédant des boutons permettant
 * d'accéder aux différentes fonctions du gestionnaire de l'alliance rebelle.
 *
 * @author Celine HAMADI
 * @version 05/05/2021
 */
public class EcranPrincipal extends JFrame
{
    public EcranPrincipal()
    {
        //Interface utilisateur (vue)
        //JFrame fenetre = new JFrame("GESTIONNAIRE DE L'ALLIANCE REBELLE");
        this.setName("GESTIONNAIRE DE L'ALLIANCE REBELLE");
        this.setSize(1200,400);
        
        //Récupération du panneau d'affichage par défaut
        Container panneau = this.getContentPane();
        //Ajoute un gestionnaire de placement au panneau
        panneau.setLayout(new GridBagLayout());
        //Gestion des composants : placement sur le panneau
        GridBagConstraints gdp = new GridBagConstraints();
        gdp.fill = GridBagConstraints.NONE;//permet d'occuper toute la place horizontalement et verticalement
        gdp.insets = new Insets(5,5,5,5);
        gdp.ipady = GridBagConstraints.CENTER;
        gdp.weightx = 3; //nbre de cases en abcisse
        gdp.weighty = 5; //nbre de cases en ordonnée 
        
        //Affichage du titre de l'application (Ecran principal)
        JLabel titre = new JLabel("GESTIONNAIRE DE L'ALLIANCE REBELLE");
        gdp.gridwidth = 3;
        gdp.gridx = 0;
        gdp.gridy = 0;
        panneau.add(titre, gdp);
                
        //Affichage du titre "Recrutement" au-dessus du bouton "Nouveau Pilote"
        JLabel tRecrutement = new JLabel("Recrutement");
        gdp.gridwidth = 1;
        gdp.gridx = 0;
        gdp.gridy = 1;
        panneau.add(tRecrutement, gdp);
        //Ajout d'un bouton de "Nouveau Pilote"
        JButton bNewPilote = new JButton("Nouveau Pilote");
        gdp.gridwidth = 1;
        gdp.gridx = 0;
        gdp.gridy = 2;
        panneau.add(bNewPilote, gdp);
        
        //Affichage du titre "Chasseurs" au-dessus du bouton "Nouveau Chasseur"
        JLabel tChasseur = new JLabel("Chasseurs");
        gdp.gridwidth = 1;
        gdp.gridx = 1;
        gdp.gridy = 1;
        panneau.add(tChasseur, gdp);
        //Ajout d'un bouton de "Nouveau Chasseur"
        JButton bNewChasseur = new JButton("Nouveau Chasseur");
        gdp.gridwidth = 1;
        gdp.gridx = 1;
        gdp.gridy = 2;
        panneau.add(bNewChasseur, gdp);
        //Ajout d'un bouton de "Modification Etat du Chasseur"
        JButton bSetEtat = new JButton("Modification Etat du Chasseur");
        gdp.gridwidth = 1;
        gdp.gridx = 1;
        gdp.gridy = 3;
        panneau.add(bSetEtat, gdp);
        
        //Affichage du titre "Affectation" au-dessus du bouton "Assigner Pilote"
        JLabel tAffectation = new JLabel("Affectation");
        gdp.gridwidth = 1;
        gdp.gridx = 2;
        gdp.gridy = 1;
        panneau.add(tAffectation, gdp);
        //Ajout d'un bouton de "Nouveau Chasseur"
        JButton bNewAffectation = new JButton("Assigner Pilote");
        gdp.gridwidth = 1;
        gdp.gridx = 2;
        gdp.gridy = 2;
        panneau.add(bNewAffectation, gdp);
        
        //Affichage du titre "Affichage" à gauche de la liste déroulante des choix
        JLabel tAfficher = new JLabel("Afficher");
        gdp.gridwidth = 1;
        gdp.gridx = 0;
        gdp.gridy = 4;
        panneau.add(tAfficher, gdp);
        //Ajout d'une liste déroulante de choix
        JComboBox ldChoixAffichage = new JComboBox();
        gdp.gridwidth = 1;
        gdp.gridx = 1;
        gdp.gridy = 4;
        panneau.add(ldChoixAffichage, gdp);
        //Remplir la liste déroulante avec les choix possibles
        ldChoixAffichage.addItem("les informations de tous les chasseurs");
        ldChoixAffichage.addItem("les informations de tous les pilotes");
        ldChoixAffichage.addItem("le nombre de chasseurs opérationnels et leur identifiant");
        ldChoixAffichage.addItem("les informations d'un pilote à partir de son numéro");
        ldChoixAffichage.addItem("les informations d'un chasseur à partir de son identifiant");
        
        //Ajout d'un bouton de "OK" pour valider le choix de la liste déroulante
        JButton bOK = new JButton("OK");
        gdp.gridwidth = 1;
        gdp.gridx = 2;
        gdp.gridy = 4;
        panneau.add(bOK, gdp);
        
        
        // on doit créer et associer un gestionnaire d'évènements pour le bouton
        bNewPilote.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e) {
                   setVisible(false);
                   AddPilote addPilote = new AddPilote(EcranPrincipal.this);
                }
            });
        
        bNewChasseur.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    AddChasseur addChasseur = new AddChasseur(EcranPrincipal.this);
                }
            });
        
        bSetEtat.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    EditEtatChasseur editEtatChasseur = new EditEtatChasseur(EcranPrincipal.this);
                }
            });
        
        bNewAffectation.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                    Affectation affectation = new Affectation(EcranPrincipal.this);
                }
            });
        bOK.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String selected = String.valueOf(ldChoixAffichage.getSelectedItem());
                    if (selected == null) {selected = "";}
                    switch (selected) {
                        case "les informations de tous les chasseurs" :
                            setVisible(false);
                            AffichageListeChasseurs affichageListeChasseurs = new AffichageListeChasseurs(EcranPrincipal.this);
                            break;
                        case "les informations de tous les pilotes" :
                            setVisible(false);
                            AffichageListePilotes affichageListePilotes = new AffichageListePilotes(EcranPrincipal.this);
                            break;
                        case "le nombre de chasseurs opérationnels et leur identifiant" :
                            setVisible(false);
                            AffichageListeChasseursOperationnel affichageListeChasseursOperationnel = new AffichageListeChasseursOperationnel(EcranPrincipal.this);
                            break;
                        case "les informations d'un pilote à partir de son numéro" :
                            setVisible(false);
                            RecherchePiloteById recherchePiloteById = new RecherchePiloteById(EcranPrincipal.this);
                            break;
                        case "les informations d'un chasseur à partir de son identifiant" :
                            setVisible(false);
                            RechercheChasseurById rechercheChasseurById = new RechercheChasseurById(EcranPrincipal.this);
                            break;
                        default :
                            JOptionPane.showMessageDialog(
                                null,
                                "Erreur.",
                                "Echec",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        );
                   
            
        //Gestion tailles des polices
        Font fontDeApp = new Font("SANS_SERIF", Font.BOLD, 50);
        titre.setFont(fontDeApp);
        Font fontDeSsTitre = new Font("SANS_SERIF", Font.BOLD, 30);
        tRecrutement.setFont(fontDeSsTitre);
        tChasseur.setFont(fontDeSsTitre);
        tAffectation.setFont(fontDeSsTitre);
        tAfficher.setFont(fontDeSsTitre);
        Font font = new Font("SANS_SERIF", Font.BOLD, 20);
        bNewPilote.setFont(font);
        bNewChasseur.setFont(font);
        bSetEtat.setFont(font);
        bNewAffectation.setFont(font);
        ldChoixAffichage.setFont(font);
        bOK.setFont(font);
        
        //Alignement horizontal du texte = centré
        titre.setHorizontalAlignment(SwingConstants.CENTER);
        tRecrutement.setHorizontalAlignment(SwingConstants.CENTER);
        tChasseur.setHorizontalAlignment(SwingConstants.CENTER);
        tAffectation.setHorizontalAlignment(SwingConstants.CENTER);
        tAfficher.setHorizontalAlignment(SwingConstants.RIGHT);
        
        //Couleur arrière plan
        this.setBackground(Color.lightGray);
        
        this.setLocationRelativeTo(null);
        this.setVisible(true);

    }
}
