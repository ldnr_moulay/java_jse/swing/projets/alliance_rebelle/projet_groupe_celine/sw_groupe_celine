package InterfaceGraphique;
import Donnees.*;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JComboBox;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Fenetre permettant d'ajouter un nouvel objet chasseur
 * dans la base de donnée.
 *
 * @author Jules Flores
 * @version 03/05/2021 version 1
 */
public class AddChasseur extends JFrame
{
    /**
     * Constructeur de la fenetre 
     */
    public AddChasseur(JFrame parent){
        this.setTitle("Création d'un nouveau Chasseur");
        this.setSize(400,400);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new GridLayout(0,2));
        JLabel typeLabel = new JLabel("Type : ");
        JLabel etatLabel = new JLabel("Etat : ");
        JComboBox typeInput = new JComboBox(Chasseur.listeType);
        JComboBox etatInput = new JComboBox(Chasseur.listeEtat);;
        JButton boutonOK = new JButton("Valider");
        JButton boutonRetour = new JButton("Retour");
        this.add(typeLabel);
        this.add(typeInput);
        this.add(etatLabel);
        this.add(etatInput);
        this.add(boutonOK);
        this.add(boutonRetour);
        this.setVisible(true);
        
        boutonOK.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                boolean response = BaseDeDonnee.addChasseur(
                    (String)typeInput.getSelectedItem(),
                    (String)etatInput.getSelectedItem()
                );
                if (response){
                    JOptionPane.showMessageDialog(
                        null,
                        "Entité crée avec succes",
                        "Succès",
                        JOptionPane.INFORMATION_MESSAGE);
                    AddChasseur.this.dispose();
                    parent.setVisible(true);
                }else{
                    JOptionPane.showMessageDialog(
                        null,
                        "Erreur, donnée non conforme.",
                        "Echec",
                        JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        boutonRetour.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                dispose();
                parent.setVisible(true);
            }       
        });
    }
}
