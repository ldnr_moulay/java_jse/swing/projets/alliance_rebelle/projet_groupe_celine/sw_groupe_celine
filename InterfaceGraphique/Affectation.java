package InterfaceGraphique;
import Donnees.*;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Donnees.*;

/**
 * Fenetre permettant d'assigner les couples Chasseur/Pilote
 *
 * @author Jules Flores et Celine Hamadi
 * @version 04/05/2021 version 2
 */
public class Affectation extends JFrame
{
    /**
     * Constructeur de la fenetre 
     */
    public Affectation(JFrame parent){
        this.setTitle("Affectation Pilote/Chasseur");
        this.setSize(400,400);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new GridLayout(0,2));
        JLabel piloteLabel = new JLabel("Id du pilote : ");
        JLabel chasseurLabel = new JLabel("Id du chasseur : ");
        JTextField piloteInput = new JTextField();
        JTextField chasseurInput = new JTextField();
        JButton boutonOK = new JButton("Valider");
        JButton boutonRetour = new JButton("Retour");
        this.add(piloteLabel);
        this.add(piloteInput);
        this.add(chasseurLabel);
        this.add(chasseurInput);
        this.add(boutonOK);
        this.add(boutonRetour);
        this.setVisible(true);
        
        boutonOK.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                //Recherche de l'objet Chasseur correspondant à l'ID du chasseurInput
                String input = "";
                int idChasseur = -1 ;
                Chasseur c = null;
                try {
                    input = chasseurInput.getText(); 
                }
                catch (NullPointerException npe1) { 
                    JOptionPane.showMessageDialog(
                        null,
                        "Identifiant du chasseur inconnu",
                        "Alerte", 
                        JOptionPane.WARNING_MESSAGE
                    );
                }
                try {
                    idChasseur = Integer.valueOf(input);
                }
                catch (NumberFormatException nfe1 ) {
                    JOptionPane.showMessageDialog(
                        null,
                        "L'identifiant doit comporter des chiffres",
                        "Alerte",
                        JOptionPane.WARNING_MESSAGE
                    );
                }
                c = BaseDeDonnee.getChasseurById(idChasseur); 
                
                //Recherche de l'objet Pilote correspondant à l'ID du piloteInput
                String input2 ="" ;
                int idPilote = -1 ; 
                Pilote p = null;
                try {
                    input2 = piloteInput.getText(); 
                }
                catch (NullPointerException npe2) { 
                    JOptionPane.showMessageDialog(
                        null,
                        "Identifiant du pilote inconnu",
                        "Alerte",
                        JOptionPane.WARNING_MESSAGE
                    );
                }
                try {
                    idPilote = Integer.valueOf(input2);
                }
                catch (NumberFormatException nfe2 ) {
                    JOptionPane.showMessageDialog(
                        null,
                        "L'identifiant doit comporter des chiffres",
                        "Alerte",
                        JOptionPane.WARNING_MESSAGE
                    );
                }
                p = BaseDeDonnee.getPiloteById(idPilote); 
                
                //Vérification de l'exisance des entités
                if (c==null) {
                    JOptionPane.showMessageDialog(
                        null,
                        "Identifiant du chasseur inconnu",
                        "Alerte",
                        JOptionPane.WARNING_MESSAGE
                    );
                }
                if (p==null) {
                    JOptionPane.showMessageDialog(
                        null,
                        "Identifiant du pilote inconnu",
                        "Alerte", 
                        JOptionPane.WARNING_MESSAGE
                    );
                }
                
                if (c!= null && p!=null){                    
                    String affectation1 = "";
                    String affectation2 = "";
                    int valeurRetour = -1;
                    if (c.getIdPilote()==-1 && p.getIdChasseur()== -1 ) { 
                        //Si ni c, ni p sont déjà associés, on peut faire l'association
                        c.setIdPilote(idPilote) ;
                        p.setIdChasseur(idChasseur) ;
                        JOptionPane.showMessageDialog(
                            null,
                            "Affectation réussie ! "+affectation1 + affectation2,
                            "Information",
                            JOptionPane.INFORMATION_MESSAGE
                        );
                        Affectation.this.dispose();
                        parent.setVisible(true);
                    } else { 
                        // si c et/ou p est déjà associé    
                        // ouverture de la fenetre Alerte pop-up
                        valeurRetour = JOptionPane.showConfirmDialog(
                            null,
                            "ATTENTION : des affectations existent déjà pour les identifiants renseignés.\n Si vous confirmez la nouvelle affectation, les précédentes seront effacées. Voulez-vous continuez ?",
                            "Alerte",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE
                        );
                        // retour de la demande de modification
                        if (valeurRetour==0) {
                            //correspond au choix OUI :on efface les anciens liaison obsolete
                            if (c.getIdPilote() != -1 ) { 
                                Pilote p2 = BaseDeDonnee.getPiloteById(c.getIdPilote());
                                p2.setIdChasseur(-1);
                                affectation1 = "L'affectation précédente du Chasseur ID : "+ idPilote 
                                    + " et du Pilote ID : " + p2.getId() + " a été annulée.";
                            }
                            if (p.getIdChasseur() !=-1 ) { 
                                Chasseur c2 = BaseDeDonnee.getChasseurById(p.getIdChasseur());
                                c2.setIdPilote(-1);
                                affectation2 = "L'affectation précédente du Chasseur ID : "+c2.getId() 
                                    + " et du Pilote ID : " + idChasseur + " a été annulée.";
                            }
                            //on fait la nouvelle affectation
                            c.setIdPilote(idPilote);
                            p.setIdChasseur(idChasseur);
                            //message de confirmation
                            JOptionPane.showMessageDialog(
                                null,
                                "Affectation réussie ! "+affectation1 + affectation2,
                                "Information",
                                JOptionPane.INFORMATION_MESSAGE
                            );
                            Affectation.this.dispose();
                            parent.setVisible(true);
                        }
                    }
                }else {
                    JOptionPane.showMessageDialog(
                        null,
                        "Affectation impossible",
                        "Erreur", 
                        JOptionPane.ERROR_MESSAGE
                    ); 
                }         
            }
        });
        
        boutonRetour.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                dispose();
                parent.setVisible(true);
            }       
        });
    }
}
