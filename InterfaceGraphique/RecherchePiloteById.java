package InterfaceGraphique;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import Donnees.*;



/**
 * Ecran créé lorsque  "Afficher les informations d'un pilote à partir de son numéro
 * a été sélectionné dans la liste déroulante de l'Ecran Principal et validé.
 * Permet de lancer une recherche par identifiant du pilote et d'afficher les informations correspondantes à celui-ci.
 * 
 * @author Celine HAMADI
 * @version 05/05/2021
 */
public class RecherchePiloteById extends JFrame 
{
    public RecherchePiloteById(JFrame parent) {
        //Interface utilisateur (vue)
        this.setName("RECHERCHER LES INFORMATIONS D'UN PILOTE D'APRES SON NUMERO");
        this.setSize(700,500);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //Récupération du panneau d'affichage par défaut
        Container panneau = this.getContentPane();
        //Ajoute un gestionnaire de placement au panneau
        panneau.setLayout(new GridBagLayout()); 
        GridBagConstraints gdp = new GridBagConstraints();
        gdp.fill = GridBagConstraints.HORIZONTAL;
        gdp.insets = new Insets(5,5,5,5);
        gdp.ipady = GridBagConstraints.CENTER;
        gdp.weightx = 2; //nbre de cases en abcisse
        gdp.weighty = 5; //nbre de cases en ordonnée 
        
        JTextArea infosDemandees = new JTextArea("Vous souhaitez afficher les informations\n concernant le pilote ayant le numéro : ");
        gdp.gridwidth = 2;
        gdp.gridx = 0;
        gdp.gridy = 0;
        panneau.add(infosDemandees, gdp);
             
        JLabel numéroPilote = new JLabel("Numéro du Pilote : ");
        gdp.gridwidth = 1;
        gdp.gridx = 0;
        gdp.gridy = 1;
        panneau.add(numéroPilote, gdp);
        
        JTextField chpSaisieNumPilote = new JTextField();
        gdp.gridwidth = 1;
        gdp.gridx = 1;
        gdp.gridy = 1;
        panneau.add(chpSaisieNumPilote, gdp);
        
        
        JButton bRechPilote = new JButton("Rechercher");
        gdp.fill = GridBagConstraints.NONE;
        gdp.gridwidth = 1;
        gdp.gridx = 0;
        gdp.gridy = 2;
        panneau.add(bRechPilote, gdp);
        
        JButton bRetour = new JButton("Ecran Principal");
        gdp.fill = GridBagConstraints.NONE;
        gdp.gridwidth = 1;
        gdp.gridx = 1;
        gdp.gridy = 2;
        panneau.add(bRetour, gdp);
        
        JLabel titreRep = new JLabel("INFORMATIONS DU PILOTE");
        gdp.gridwidth = 2;
        gdp.gridx = 0;
        gdp.gridy = 3;
        panneau.add(titreRep, gdp);
        
        JTextArea trameInfoRep = new JTextArea(" Identifiant Pilote :\n Prénom :\n Nom :\n Race :\n Age :\n Identifiant du Chasseur affecté : ");
        gdp.gridwidth = 1;
        gdp.gridx = 0;
        gdp.gridy = 4;
        panneau.add(trameInfoRep, gdp);
        
        JButton bOK = new JButton("OK");
        gdp.fill = GridBagConstraints.NONE;
        gdp.gridwidth = 2;
        gdp.gridx = 0;
        gdp.gridy = 5;
        panneau.add(bOK, gdp);
        bOK.setVisible(false);
        
        
        //Couleur arrière plan
        this.setBackground(Color.lightGray);
        infosDemandees.setBackground(Color.lightGray);
        
        //Gestion des polices
        Font fontDeSsTitre = new Font("SANS_SERIF", Font.BOLD, 20);
        infosDemandees.setFont(fontDeSsTitre);
        titreRep.setFont(fontDeSsTitre);
        
        Font font = new Font("SANS_SERIF", Font.BOLD, 15);
        numéroPilote.setFont(font);
        bRechPilote.setFont(font);
        bRetour.setFont(font);
        trameInfoRep.setFont(font);
        bOK.setFont(font);
        
        bRechPilote.addActionListener(new ActionListener()
             {
               @Override
               public void actionPerformed(ActionEvent e) {
                String input ="" ;
                int idPilote = -1 ; 
                String idChasseur ;
                try {
                    input = chpSaisieNumPilote.getText(); 
                }
                catch (NullPointerException npe) { 
                    JOptionPane.showMessageDialog(null,"Identifiant du pilote inconnu","Alerte", JOptionPane.WARNING_MESSAGE);
                }
                try {
                    idPilote = Integer.valueOf(input);
                }
                catch (NumberFormatException nfe ) {
                    JOptionPane.showMessageDialog(null,"L'identifiant doit comporter des chiffres","Alerte", JOptionPane.WARNING_MESSAGE);
                }
                Pilote p = BaseDeDonnee.getPiloteById(idPilote); 
                if (p==null) {
                    JOptionPane.showMessageDialog(null,"Identifiant du pilote inconnu","Alerte", JOptionPane.WARNING_MESSAGE);}
                    else {
                    if (p.getIdChasseur()==-1) {
                        idChasseur = "pas de chasseur affecté";}
                        else {idChasseur = ""+p.getIdChasseur();}
                    JTextArea infoRep = new JTextArea(p.getId()+"\n"+ p.getPrenom()+"\n"+ p.getNom() +"\n" + p.getRace() +"\n" + p.getAge() +"\n" + idChasseur);
                    gdp.gridwidth = 1;
                    gdp.gridx = 1;
                    gdp.gridy = 4;
                    panneau.add(infoRep, gdp);
                    infoRep.setFont(font);
                
                    bOK.setVisible(true);}
              }
            });
        bRetour.addActionListener(new ActionListener()
            {
               @Override
               public void actionPerformed(ActionEvent e) {
                   parent.setVisible(true); 
                   RecherchePiloteById.this.dispose();
               }
            });
            
        bOK.addActionListener(new ActionListener()
            {
               @Override
               public void actionPerformed(ActionEvent e) {
                   parent.setVisible(true); 
                   RecherchePiloteById.this.dispose();
               }
            });
            
        
        chpSaisieNumPilote.setPreferredSize(new Dimension(50,50));
        
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
