package InterfaceGraphique;
import Donnees.*;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JComboBox;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.util.HashSet;


/**
 * Fenetre affichant la liste de tous les pilotes.
 *
 * @author Flores Jules
 * @version 05/05/2021
 */
public class AffichageListePilotes extends JFrame
{
   /**
    * Constructeur de la fenetre
    */ 
   public AffichageListePilotes(JFrame parent){
        HashSet<Pilote> list = BaseDeDonnee.getTousPilotes();
        this.setTitle("Liste des Pilotes");
        this.setSize(600, 500);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new BorderLayout());
        
        JPanel top = new JPanel();
        top.setLayout(new GridLayout(0,6));
        top.add(new JLabel("Id"));
        top.add(new JLabel("Nom"));
        top.add(new JLabel("Prenom"));
        top.add(new JLabel("Race"));
        top.add(new JLabel("Age"));
        top.add(new JLabel("IdChasseur"));
        
        JPanel center = new JPanel();
        center.setLayout(new GridLayout(0,6));
        for (Pilote p : list) {
            center.add(new JLabel(""+p.getId()));
            center.add(new JLabel(p.getNom())); 
            center.add(new JLabel(p.getPrenom()));
            center.add(new JLabel(p.getRace())); 
            center.add(new JLabel(""+p.getAge())); 
            center.add(new JLabel( p.getIdChasseur() == -1 ? "non affecté" : ""+p.getIdChasseur() ));
        }        
        
        JButton boutonOK = new JButton("Retour");
        
        this.add(top, BorderLayout.PAGE_START);
        this.add(new JScrollPane(center), BorderLayout.CENTER);
        this.add(boutonOK, BorderLayout.PAGE_END);
        this.setVisible(true);
        
        boutonOK.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                parent.setVisible(true);
                AffichageListePilotes.this.dispose();
            }       
        });
    }
     
}
