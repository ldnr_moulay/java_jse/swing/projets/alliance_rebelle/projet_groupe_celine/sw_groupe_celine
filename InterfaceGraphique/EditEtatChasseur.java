package InterfaceGraphique;
import Donnees.*;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JComboBox;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import javax.swing.JPanel;


/**
 * Fenetre permettant de mettre a jour l'état d'un chasseur.
 *
 * @author Jules Flores
 * @version 05/05/2021 
*/
public class EditEtatChasseur extends JFrame
{
    private int id;
    /**
     * Constructeur de la fenetre 
     */
    public EditEtatChasseur(JFrame parent){
        this.setTitle("Recherche d'un chasseur");
        this.setSize(400,400);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        JPanel search = new JPanel(new GridLayout(0,2));
        JLabel idLabel = new JLabel("Id du chasseur à modifier :");
        JTextField idInput = new JTextField();
        JButton boutonSearch = new JButton("Recherche");
        JButton boutonRetour = new JButton("Retour");
        search.add(idLabel);
        search.add(idInput);
        search.add(boutonSearch);
        search.add(boutonRetour);
        
        JPanel result = new JPanel(new GridLayout(0,2));
        JLabel etatLabel = new JLabel("Etat : ");
        JComboBox etatInput = new JComboBox(Chasseur.listeEtat);
        JButton boutonOK = new JButton("Valider");
        JButton boutonRetourSearch = new JButton("Retour");
        result.add(etatLabel);
        result.add(etatInput);
        result.add(boutonOK);
        result.add(boutonRetourSearch);
        
        this.add(search);
        this.setVisible(true);
        
        boutonSearch.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                //vérif id
                id = -1;
                try {
                    if (idInput.getText() != null){
                        id = Integer.parseInt(idInput.getText());
                    }
                } catch (NumberFormatException nfe){
                    //on ignore car avec id = -1 le pilote sera null
                }
                if (BaseDeDonnee.getChasseurById(id) != null) {
                    //affichage pour edit
                    remove(search);
                    search.setVisible(false);
                    add(result);
                    result.setVisible(true);
                    setTitle("Editer le chasseur n°" + id);
                    //afficher la valeur actuel dans la combobox ?
                    invalidate();
                    validate();
                } else {
                    JOptionPane.showMessageDialog(
                        null,
                        "Erreur, aucun pilote n'a cette Id.",
                        "Echec",
                        JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        boutonRetourSearch.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                remove(result);
                result.setVisible(false);
                add(search);
                search.setVisible(true);
                setTitle("Recherche d'un chasseur");
                invalidate();
                validate();
            }       
        });
        boutonOK.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                //changer l'état
                String etat = (String)etatInput.getSelectedItem();
                boolean response = BaseDeDonnee.setEtatChasseurById(id, etat);
                if (response){
                    JOptionPane.showMessageDialog(
                        null,
                        "Entité modifiée avec succès !",
                        "Succès",
                        JOptionPane.INFORMATION_MESSAGE);
                    EditEtatChasseur.this.dispose();
                    parent.setVisible(true);
                }else{
                    JOptionPane.showMessageDialog(
                        null,
                        "Erreur, échec de la modification.",
                        "Echec",
                        JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        boutonRetour.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                dispose();
                parent.setVisible(true);
            }       
        });
    }
}
