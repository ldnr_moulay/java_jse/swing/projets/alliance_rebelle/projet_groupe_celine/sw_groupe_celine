package InterfaceGraphique;
import Donnees.*;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JComboBox;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.util.HashSet;



/**
 * Fenetre affichant la liste de tous les chasseurs.
 *
 * @author Flores Jules
 * @version 05/05/2021
 */
public class AffichageListeChasseurs extends JFrame
{
    /**
     * Constructeur de la fenetre 
     */
    public AffichageListeChasseurs(JFrame parent){
        HashSet<Chasseur> list = BaseDeDonnee.getTousChasseurs();
        this.setTitle("Liste des Chasseurs");
        this.setSize(400, 400);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new BorderLayout());
        
        JPanel top = new JPanel();
        top.setLayout(new GridLayout(0,4));
        top.add(new JLabel("Id"));
        top.add(new JLabel("Type"));
        top.add(new JLabel("Etat"));
        top.add(new JLabel("IdPilote"));
        
        JPanel center = new JPanel();
        center.setLayout(new GridLayout(0,4));
        for (Chasseur c : list) {
            center.add(new JLabel(""+c.getId()));
            center.add(new JLabel(c.getType()));
            center.add(new JLabel(c.getEtat()));
            center.add(new JLabel( c.getIdPilote() == -1 ? "non affecté" : ""+c.getIdPilote() ));
        }
        
        JButton boutonOK = new JButton("Retour");
        
        this.add(top, BorderLayout.PAGE_START);
        this.add(new JScrollPane(center), BorderLayout.CENTER);
        this.add(boutonOK, BorderLayout.PAGE_END);
        this.setVisible(true);
        
        boutonOK.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                parent.setVisible(true);
                AffichageListeChasseurs.this.dispose();
            }       
        });
    }
    
}
    

