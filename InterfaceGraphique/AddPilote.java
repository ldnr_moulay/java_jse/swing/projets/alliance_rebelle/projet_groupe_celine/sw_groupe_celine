package InterfaceGraphique;
import Donnees.*;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JFrame;
import javax.swing.JComboBox;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Fenetre permettant d'ajouter un nouvel objet pilote
 * dans la base de donnée.
 *
 * @author Jules Flores
 * @version 03/05/2021 version 1
 */
public class AddPilote extends JFrame
{
    /**
     * Constructeur de la fenetre 
     */
    public AddPilote(JFrame parent){
        this.setTitle("Création d'un nouveau Pilote");
        this.setSize(400,400);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new GridLayout(0,2));
        JLabel prenomLabel = new JLabel("Prénom : ");
        JLabel nomLabel = new JLabel("Nom : ");
        JLabel raceLabel = new JLabel("Race : ");
        JLabel ageLabel = new JLabel("Age : ");
        JTextField prenomInput = new JTextField();
        JTextField nomInput = new JTextField();
        JComboBox raceInput = new JComboBox(Pilote.listeRaces);
        JTextField ageInput = new JTextField();
        JButton boutonOK = new JButton("Valider");
        JButton boutonRetour = new JButton("Retour");
        this.add(prenomLabel);
        this.add(prenomInput);
        this.add(nomLabel);
        this.add(nomInput);
        this.add(raceLabel);
        this.add(raceInput);
        this.add(ageLabel);
        this.add(ageInput);
        this.add(boutonOK);
        this.add(boutonRetour);
        this.setVisible(true);
        
        boutonOK.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if (prenomInput.getText() != null && nomInput.getText() != null && ageInput.getText() != null){
                    String prenom = prenomInput.getText();
                    String nom = nomInput.getText();
                    String race = String.valueOf(raceInput.getSelectedItem());
                    int age = -1;
                    try {
                        age = Integer.parseInt(ageInput.getText());
                    } catch (NumberFormatException nfe){
                        //on ignore car le constructeur marche pas avec -1 ?
                    }
                    boolean response = BaseDeDonnee.addPilote(prenom, nom, race, age);
                    if (response){
                        JOptionPane.showMessageDialog(
                            null,
                            "Entité crée avec succes",
                            "Succès",
                            JOptionPane.INFORMATION_MESSAGE);
                        AddPilote.this.dispose();
                        parent.setVisible(true);
                    }else{
                        JOptionPane.showMessageDialog(
                            null,
                            "Erreur, donnée non conforme.",
                            "Echec",
                            JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(
                        null,
                        "Erreur, champ non rempli.",
                        "Echec",
                        JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        
        boutonRetour.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                dispose();
                parent.setVisible(true);
            }       
        });
    }
}
