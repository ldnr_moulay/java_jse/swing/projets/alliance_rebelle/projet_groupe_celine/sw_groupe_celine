package InterfaceGraphique;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import Donnees.*;


/**
 * Ecran créé lorsque  "Afficher les informations d'un chasseur à partir de son identifiant
 * a été sélectionné dans la liste déroulante de l'Ecran Principal et validé.
 * Permet de lancer une recherche par identifiant du chasseur et d'afficher les informations correspondantes à celui-ci.
 *
 * @author Celine HAMADI
 * @version 05/05/2021
 */
public class RechercheChasseurById extends JFrame
{
    public RechercheChasseurById(JFrame parent) {
        //Interface utilisateur (vue)
        this.setName("RECHERCHER LES INFORMATIONS D'UN CHASSEUR D'APRES SON NUMERO");
        this.setSize(700,500);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //Récupération du panneau d'affichage par défaut
        Container panneau = this.getContentPane();
        //Ajoute un gestionnaire de placement au panneau
        panneau.setLayout(new GridBagLayout()); 
        GridBagConstraints gdp = new GridBagConstraints();
        gdp.fill = GridBagConstraints.HORIZONTAL;
        gdp.insets = new Insets(5,5,5,5);
        gdp.ipady = GridBagConstraints.CENTER;
        gdp.weightx = 2; 
        gdp.weighty = 5; 
        
        JTextArea infosDemandees = new JTextArea("Vous souhaitez afficher les informations\n concernant le chasseur ayant l'identifiant : ");
        gdp.gridwidth = 2;
        gdp.gridx = 0;
        gdp.gridy = 0;
        panneau.add(infosDemandees, gdp);
             
        JLabel numéroChasseur = new JLabel("Identifiant du Chasseur : ");
        gdp.gridwidth = 1;
        gdp.gridx = 0;
        gdp.gridy = 1;
        panneau.add(numéroChasseur, gdp);
        
        JTextField chpSaisieNumChasseur = new JTextField();
        gdp.gridwidth = 1;
        gdp.gridx = 1;
        gdp.gridy = 1;
        panneau.add(chpSaisieNumChasseur, gdp);
        
        
        JButton bRechChasseur = new JButton("Rechercher");
        gdp.fill = GridBagConstraints.NONE;
        gdp.gridwidth = 1;
        gdp.gridx = 0;
        gdp.gridy = 2;
        panneau.add(bRechChasseur, gdp);
        
        JButton bRetour = new JButton("Ecran Principal");
        gdp.fill = GridBagConstraints.NONE;
        gdp.gridwidth = 1;
        gdp.gridx = 1;
        gdp.gridy = 2;
        panneau.add(bRetour, gdp);
        
        JLabel titreRep = new JLabel("INFORMATIONS DU CHASSEUR");
        gdp.gridwidth = 2;
        gdp.gridx = 0;
        gdp.gridy = 3;
        panneau.add(titreRep, gdp);
        
        JTextArea trameInfoRep = new JTextArea(" Identifiant Chasseur :\n Type :\n Etat :\n Identifiant du Pilote affecté : ");
        gdp.gridwidth = 1;
        gdp.gridx = 0;
        gdp.gridy = 4;
        panneau.add(trameInfoRep, gdp);
        
        JButton bOK = new JButton("OK");
        gdp.fill = GridBagConstraints.NONE;
        gdp.gridwidth = 2;
        gdp.gridx = 0;
        gdp.gridy = 5;
        panneau.add(bOK, gdp);
        bOK.setVisible(false);
        
       
        //Couleur arrière plan
        this.setBackground(Color.lightGray);
        infosDemandees.setBackground(Color.lightGray);
        
        //Gestion des polices
        Font fontDeSsTitre = new Font("SANS_SERIF", Font.BOLD, 20);
        infosDemandees.setFont(fontDeSsTitre);
        titreRep.setFont(fontDeSsTitre);
        
        Font font = new Font("SANS_SERIF", Font.BOLD, 15);
        numéroChasseur.setFont(font);
        bRechChasseur.setFont(font);
        bRetour.setFont(font);
        trameInfoRep.setFont(font);
        bOK.setFont(font);
        
        bRechChasseur.addActionListener(new ActionListener()
             {
               @Override
               public void actionPerformed(ActionEvent e) {
                String input = "";
                int idChasseur = -1 ; 
                String idPilote ;
                try {
                    input = chpSaisieNumChasseur.getText(); 
                }
                catch (NullPointerException npe) { 
                    JOptionPane.showMessageDialog(null,"Identifiant du chasseur inconnu","Alerte", JOptionPane.WARNING_MESSAGE);
                }
                try {
                    idChasseur = Integer.valueOf(input);
                }
                catch (NumberFormatException nfe ) {
                    JOptionPane.showMessageDialog(null,"L'identifiant doit comporter des chiffres","Alerte", JOptionPane.WARNING_MESSAGE);
                }
                Chasseur c = BaseDeDonnee.getChasseurById(idChasseur); 
                if (c==null) {
                    JOptionPane.showMessageDialog(null,"Identifiant du chasseur inconnu","Alerte", JOptionPane.WARNING_MESSAGE);}
                    else {
                    if (c.getIdPilote()==-1) {
                        idPilote = "pas de pilote affecté";}
                        else {idPilote = ""+c.getIdPilote();}
                    JTextArea infoRep = new JTextArea(c.getId()+"\n"+ c.getType()+"\n"+ c.getEtat() +"\n"+ idPilote);
                    gdp.gridwidth = 1;
                    gdp.gridx = 1;
                    gdp.gridy = 4;
                    panneau.add(infoRep, gdp);
                    infoRep.setFont(font);
                
                    bOK.setVisible(true);}
              }
            });
        bRetour.addActionListener(new ActionListener()
            {
               @Override
               public void actionPerformed(ActionEvent e) {
                   parent.setVisible(true);
                   RechercheChasseurById.this.dispose();
               }
            });
            
        bOK.addActionListener(new ActionListener()
            {
               @Override
               public void actionPerformed(ActionEvent e) {
                   parent.setVisible(true); 
                   RechercheChasseurById.this.dispose();
               }
            });
           
        
        chpSaisieNumChasseur.setPreferredSize(new Dimension(50,50));
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}

