package InterfaceGraphique;
import Donnees.*;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JComboBox;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.util.HashSet;


/**
 * Fenetre affichant la liste des chasseurs opérationnels.
 *
 * @author Flores Jules
 * @version 05/05/2021
 */
public class AffichageListeChasseursOperationnel extends JFrame
{
    
    /**
     * Constructeur de la fenetre 
     */
    public AffichageListeChasseursOperationnel(JFrame parent){
        HashSet<Chasseur> list = BaseDeDonnee.getListeOperationnels();
        int nbOperationnels = BaseDeDonnee.getNbOperationnels();
        int longueur = 300; // 100 + list.size() * 100;
        this.setTitle("Liste des Chasseurs Opérationnel");
        this.setSize(200, longueur);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(new BorderLayout());
        
        JPanel top = new JPanel();
        top.setLayout(new GridLayout(0,2));
        top.add(new JLabel("Id"));
        top.add(new JLabel("Type"));
        
        JPanel center = new JPanel();
        center.setLayout(new GridLayout(0,2));
        for (Chasseur c : list) {
            center.add(new JLabel(""+c.getId()));
            center.add(new JLabel(c.getType()));
        }        
        
        JPanel nbOperation = new JPanel();
        nbOperation.setLayout(new GridLayout(0,1));
        nbOperation.add(new JLabel("Nombre de chasseurs opérationnels :"));
        nbOperation.add(new JLabel(""+nbOperationnels));
        nbOperation.add(top);
        
        JButton boutonOK = new JButton("Retour");
        
        this.add(nbOperation, BorderLayout.PAGE_START);
        this.add(new JScrollPane(center), BorderLayout.CENTER);
        this.add(boutonOK, BorderLayout.PAGE_END);
        this.setVisible(true);
        
        boutonOK.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                parent.setVisible(true);
                AffichageListeChasseursOperationnel.this.dispose();
            }       
        });
    }
 
}
