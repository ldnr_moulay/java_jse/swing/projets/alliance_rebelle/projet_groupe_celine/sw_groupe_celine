 import InterfaceGraphique.*;
 import Donnees.*;
 import javax.swing.SwingUtilities;

/**
 *  classe Application
 *
 * @author Flores Jules
 * @version 05/05/2021
 */
public class Application
{
    /**
     * Lancement de l'application
     */
    public static void main(){
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                fillBdd();
                EcranPrincipal test = new EcranPrincipal();
            }
        });
    }
    /**
     * Création de pilotes et chasseurs de base
     */
    public static void fillBdd() {
        BaseDeDonnee base = new BaseDeDonnee();
        
        base.addChasseur("X-Wing","operationnel");
        base.addChasseur("Y-Wing","maintenance");
        base.addChasseur("X-Wing","operationnel");
        base.addChasseur("X-Wing","operationnel");
        base.addChasseur("X-Wing","détruit");
        base.addChasseur("X-Wing","operationnel");
        base.addChasseur("Y-Wing","operationnel");
        base.addChasseur("X-Wing","operationnel");
        base.addChasseur("Y-Wing","operationnel");
        base.addChasseur("X-Wing","operationnel");
        base.addChasseur("X-Wing","operationnel");
        base.addChasseur("Y-Wing","détruit");
        base.addChasseur("X-Wing","operationnel");
        base.addChasseur("X-Wing","operationnel");
        base.addChasseur("X-Wing","maintenance");
        base.addChasseur("X-Wing","operationnel");
        base.addChasseur("Y-Wing","operationnel");
        base.addChasseur("Y-Wing","maintenance");
        base.addChasseur("X-Wing","operationnel");
        
        base.addPilote("Adrien","Dupond","Humains",36);
        base.addPilote("Bastien","Dupond","Humains",36);
        base.addPilote("Charles","Dupond","Humains",36);
        base.addPilote("Damien","Dupond","Kiffars",396);
        base.addPilote("Adrien","Dupond","Humains",306);
        base.addPilote("Adrien","Durand","Humains",36);
        base.addPilote("Adrien","Dupond","Nagais",36);
        base.addPilote("Adrien","Dupond","Humains",36);
        base.addPilote("Adrien","Dupond","Humains",536);
        base.addPilote("Adrien","Durand","Humains",360);
        base.addPilote("Adrien","Dupond","Humains",36);
        base.addPilote("Adrien","Dupond","Niktos",36);
        base.addPilote("Adrien","Dupont","Humains",36);
        base.addPilote("Adrien","Dupond","Humains",36);
        base.addPilote("Adrien","Dupond","Humains",36);
        base.addPilote("Adrien","Dupont","Wookies",36);
        base.addPilote("Adrien","Dupond","Humains",36);
        base.addPilote("Adrien","Dupond","Ongrees",362);
        base.addPilote("Adrien","Dupond","Humains",36);
        base.addPilote("Adrien","Dupond","Humains",36);
        base.addPilote("Adrien","Dupond","Humains",356);
        base.addPilote("Adrien","Dupond","Togrutas",36);
        base.addPilote("Adrien","Dupond","Humains",36);
        base.addPilote("Adrien","Dupond","Humains",365);
        base.addPilote("Adrien","Dupond","Humains",36);
        base.addPilote("Adrien","Dupond","Humains",736);
        base.addPilote("Adrien","Dupond","Humains",368);
        base.addPilote("Adrien","Dupond","Humains",36);
    }
}
