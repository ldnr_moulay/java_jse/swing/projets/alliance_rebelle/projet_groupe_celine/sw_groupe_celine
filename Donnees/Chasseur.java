package Donnees;


/**
 * classe Chasseur qui permet de créer et modifier les objets chasseurs
 *
 * @ Catusse Thomas
 * @ 05/05/2021
 */
public class Chasseur
{
    /**
     *  Déclaration des variables de la classe
     */
    private static int generator = 0;
    public static String[] listeType = {"X-Wing","Y-Wing"};
    public static String[] listeEtat = {"operationnel","maintenance","détruit"};
    
    private int id;
    private String type;
    private String etat;
    private int idPilote = -1; //valeur par defaut (non affecté)
    
    /**
     *  Constructeur de la classe Chasseur
     */
    public Chasseur (String type, String etat) {
        this.id = generator;
        generator++;
        this.type = type;
        this.etat = etat;
        this.idPilote = idPilote;
    }
    
    
    /**
     *  Modification de l'état d'un objet chasseur
     */
    public void setEtatChasseur(String etat) {
        this.etat = etat;    
    }
    
    
    /**
     *  Retourne l'identifiant d'un objet chasseur
     */
    public int getId() {
        return id;
    }
    
    
    /**
     *  Retourne le type d'un objet chasseur
     */
    public String getType() {
        return type;
    }
    
    
    /**
     *  Retourne l'état d'un objet chasseur
     */
    public String getEtat() {
        return etat;
    }
    
    /**
     *  Retourne l'identifiant pilote affecté à un objet chasseur
     */
    public int getIdPilote() {
        return idPilote;
    }
    
    /**
     *  Modification de l'identifiant du pilote affecté à un objet chasseur
     */
    public void setIdPilote(int idPilote) {
        this.idPilote = idPilote;    
    }
}
