package Donnees;
import java.util.HashSet;
import java.util.Arrays;

/**
 * classe BaseDeDonnee qui permet de stocker les objets pilotes et chasseurs
 *
 * @ Catusse Thomas
 * @ 05/05/2021
 */
public class BaseDeDonnee
{
    /**
     * Déclarations des objets chasseurs et pilotes
     */ 
    private static HashSet<Chasseur> chasseurs;
    private static HashSet<Pilote> pilotes;

    
        /**
         *  Création des objets chasseurs et pilotes
         */
        public BaseDeDonnee() {
            chasseurs = new HashSet<Chasseur>();
            pilotes = new HashSet<Pilote>();
        }
    
        
        /** Ajout d'un chasseur, paramètre type et état
         *  fait appel au constructeur de la classe Chasseur    
         */
        public static boolean addChasseur(String type, String etat){
            boolean response = false;
            if (Arrays.toString(Chasseur.listeType).contains(type) 
                    && Arrays.toString(Chasseur.listeEtat).contains(etat))
            {
                Chasseur chasseur = new Chasseur(type, etat);
                chasseurs.add(chasseur);
                response = true;
            } 
            return response;
        }
        
        
        /**
         *  Récupération d'un objet chasseur selon son identifiant
         */
        public static Chasseur getChasseurById(int id){
            for (Chasseur c : chasseurs){
                if (c.getId() == id){
                      return c;  
                }
            }
            return null;
        }
        
        
        /**
         *  Récupération de tous les objets chasseurs
         */
        public static HashSet<Chasseur> getTousChasseurs() {
            return chasseurs;
        }
        
        
        /**
         *  Changement de l'état d'un objet chasseur selon son identifiant
         */
        public static boolean setEtatChasseurById(int idChasseur, String etat){
            for (Chasseur c : chasseurs){
                if (c.getId()== idChasseur){
                    c.setEtatChasseur(etat);
                    return true;
                }
            }
            return false;
        }
        
        
        /**
         *  Récupération de la liste des objets chasseurs dont l'état est opérationnel
         */
        public static HashSet<Chasseur> getListeOperationnels(){
            HashSet<Chasseur> liste = new HashSet<>();
            for (Chasseur c : chasseurs){
                if (c.getEtat().equals("operationnel")){
                    liste.add(c);     
                }
            }
            return liste;
        }
        
        
        /**
         *  Récupération du nombre d'objets chasseurs dont l'état est opérationnel
         */
        public static int getNbOperationnels(){
            return getListeOperationnels().size();
        }
        
        
        /**
         *  Ajout d'un objet pilotes
         */
        public static boolean addPilote(String prenom, String nom, String race, int age) {
            boolean reponse = false;
            if(age>10 && age<800 && !prenom.equals("") && !nom.equals("") 
                && Arrays.toString(Pilote.listeRaces).contains(race)) 
            {    
                Pilote pilote = new Pilote(prenom, nom, race, age);
                pilotes.add(pilote);
                reponse = true;
            }
            return reponse;
        }
        
        
        /**
         *  Récupération d'un objet pilotes selon son identifiant
         */
        public static Pilote getPiloteById(int id) {
            for (Pilote p : pilotes){
                if (p.getId() == id){
                    return p;
                }
            }
            return null;
        }
        
        
        /**
         *  Récupération de tous les objets pilotes
         */
        public static HashSet<Pilote> getTousPilotes(){
            return pilotes;
        }
        
        
        /**
         *  Affectation d'un objet pilotes a un objet chasseurs
         */
        public static boolean affectPiloteToChasseur(int idPilote, int idChasseur){
            boolean response = false;
            if (getPiloteById(idPilote)!= null && getChasseurById(idChasseur)!=null){
                // code a faire 
            }            
            return response;
        }
}
