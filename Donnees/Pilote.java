package Donnees;


/**
 * classe Pilote qui permet de créer et modifier les objets pilotes
 *
 * @ Catusse Thomas
 * @ 05/05/2021
 */
public class Pilote
{
    /**
     *  Déclaration des variables de la classe
     */
    private static int generator = 0;
    public static String[] listeRaces = {"Chalactéens","Chiss","Humains","Ithoriens","Mirialans","Kel'Dor","Kiffars",
        "Miralukas","Nagais","Neimoidiens","Niktos","Noghris","Ongrees","Pau'ans","Quermiens","Rakata",
        "Rodiens","Thisspasiens","Togrutas","Wookies","Wronians","Zabraks"};
    
    private int id;
    private String prenom;
    private String nom;
    private String race;
    private int age;
    private int idChasseur = -1; //valeur par defaut (non affecté)
    
    
    /**
     *  Constructeur de la classe Pilote
     */
    public Pilote (String prenom, String nom, String race, int age){
        this.id = generator;
        generator++;
        this.prenom = prenom;
        this.nom = nom;
        this.race = race;
        this.age = age;
        this.idChasseur = idChasseur;
    }
    
    
    /**
     *  Retourne l'identifiant d'un objet pilote
     */
    public int getId() {
        return id;
    }
    
    
    /**
     *  Retourne le prenom d'un objet pilote
     */
    public String getPrenom() {
        return prenom;
    }
    
    
    /**
     *  Retourne le nom d'un objet pilote
     */
    public String getNom() {
        return nom;
    }
    
    
    /**
     *  Retourne la race d'un objet pilote
     */
    public String getRace() {
        return race;
    }
    
    
    /**
     *  Retourne l'age d'un objet pilote
     */
    public int getAge() {
        return age;
    }
    
    
    /**
     *  Retourne l'identifiant chasseur d'un objet pilote
     */
    public int getIdChasseur() {
        return idChasseur;
    }
    
    
    /**
     *  Modification de l'identifiant chasseur affecté à un objet pilote
     */
    public void setIdChasseur(int idChasseur) {
        this.idChasseur = idChasseur;    
    }
}
